# -*- coding: utf-8 -*-

import json, time
from flask import request

from database import Database
import app_mebbix_event
import app_mebbix_count
import app_mebbix_command

db = None

minerlist_struct = {
	'id': 0,
        'name' : '',
	'create_time' : 0,
	'modify_time' : 0,
	'version' : 0,
	'cpu' : '',
	'board' : '',
	'graphics' : '',
	'ip' : '',
	'mac' : '',
	'last_refresh_time' : 0,
	'last_miner_refresh_time': 0,
	'miner_state' : False,
	'miner_version' : '',
	'miner_running_time' : 0,
	'mining_mode' : 0,
	'mining_coin1' : 0,
	'mining_hash1_sum' : 0,
	'mining_hash1' : [],
	'mining_coin2' : 0,
	'mining_hash2_sum' : 0,
	'mining_hash2' : [],
	'graphics_temperate' : [],
	'graphics_count' : 0,
	'pool_address' : [],
	'pool_share' : [],
	'graphics_max' : None,
	'memo' : None,
}

minerlist = {}

mineridlist = {}

def init():
	global db, minerlist, mineridlist

	print('app_mebbix_agent init')
	db = Database()

	tempdata = db.miner_detail_get()

	for a in tempdata:
		hostname = a[1]
		id = a[0]
		newminer = minerlist_struct.copy()
		newminer['id'] = id
		newminer['name'] = hostname
		newminer['create_time'] = a[2]
		newminer['modify_time'] = a[3]
		newminer['cpu'] = a[4]
		newminer['board'] = a[5]
		newminer['graphics'] = a[6]
		newminer['ip'] = a[7]
		newminer['mac'] = a[8]
		#todo refactoring
		newminer['graphics_max'] = app_mebbix_count.get(id, 100)
		newminer['memo'] = app_mebbix_count.get(id, 101)

		app_mebbix_command.miner_add(id)

		minerlist[hostname] = newminer
		mineridlist[id] = hostname

def poweron():
	recvdata = json.loads(request.stream.read())

	if recvdata:
		#app_mebbix_event.poweron(recvdata['comname'])
		if recvdata['comname'] in minerlist.keys():
			#pc 정보 변경시 이벤트로그 남기기
			miner_update(recvdata['comname'], recvdata['minerinfo'])
			
		else:
			miner_new(recvdata['comname'], recvdata['minerinfo'])

	else:
		return '뭐함???'

	return '{"result":"ok"}'

def reboot():
	#reboot시 event 작성
	1

def alive():
	global minerlist

	#alive check할 시
	#claymore에서 값을 받아서 client에서 server로 값전송
	#일부 정보는 db갱신 일부는 메모리에만
	#db에 등록되지 않은 마이너 튕겨내기

	data = request.stream.read()
	current_state = json.loads(data)
	
	hostname = current_state['comname']

	result = {}

	if minerlist.get(hostname, None) is None:
		result['result'] = 'noregister'
		return json.dumps(result)

	if current_state['miner_state']:
		miner_data = json.loads(current_state['miner_state'])

		miner_name = current_state.get('miner_name', None)

		if miner_name == 'ewbf_zcash':
			miner_state_update_ewbf(hostname, miner_data)

		elif miner_name == 'claymore_monero':
			miner_state_update_claymore_monero(hostname, miner_data)

		elif miner_name == 'claymore_ethereum' or miner_name is None:
			#miner_name은 agent version 4부터
			miner_state_update_claymore(hostname, miner_data)

		else:
			minerlist[hostname]['miner_state'] = False
			#추후 이벤트 로그 발생

		minerlist[hostname]['last_miner_refresh_time'] = int(time.time())

	if minerlist[hostname]['graphics_max']['data'] < minerlist[hostname]['graphics_count']:
		app_mebbix_count.update(minerlist[hostname]['id'], 100, minerlist[hostname]['graphics_count'])

	minerlist[hostname]['last_refresh_time'] = int(time.time())

	result['command'] = app_mebbix_command.miner_command_get(minerlist[hostname]['id'])
	result['result'] = 'OK'
	return json.dumps(result)
	
def miner_new(hostname, minerinfo):
	global minerlist, mineridlist

	print 'new miner %s' % hostname

	newminer = minerlist_struct.copy()
	newminer['name'] = hostname
	newminer['create_time'] = newminer['modify_time'] = int(time.time())

	for a in minerinfo.keys():
		newminer[a] = minerinfo[a]

	minerlist[hostname] = newminer
	
	db.miner_detail_add(hostname, minerinfo)

	buf = db.miner_detail_get_id_by_name(hostname)
	id = buf[0]

	minerlist[hostname]['id'] = id
	minerlist[hostname]['graphics_max'] = app_mebbix_count.get(id, 100)
	minerlist[hostname]['memo'] = app_mebbix_count.get(id, 101)
	mineridlist[id] = hostname
	
	app_mebbix_event.event_add(minerlist[hostname]['id'], 100, int(time.time()), int(time.time()))

	app_mebbix_command.miner_add(id)

def miner_delete(hostname):
	global minerlist, mineridlist

	id = minerlist[hostname]['id']

	db.miner_delete(int(id))

	del minerlist[hostname]
	del mineridlist[id]

def miner_update(hostname, minerinfo):
	global minerlist

	diff = 0

	for a in minerinfo.keys():
		if minerlist[hostname][a] != minerinfo[a]:
			diff = diff + 1

			minerlist[hostname][a] = minerinfo[a]

	if diff > 0:
		print 'miner update %s' % hostname
		db.miner_detail_update(hostname, minerinfo)


def miner_state_update_claymore(hostname, miner_state):
	global minerlist

	minerlist[hostname]['mining_coin1'] = 100
	minerlist[hostname]['mining_coin2'] = 101

	minerlist[hostname]['miner_state'] = True

	minerlist[hostname]['miner_version'] = miner_state['result'][0]
	minerlist[hostname]['miner_running_time'] = int(time.time()) - int(miner_state['result'][1]) * 60
	minerlist[hostname]['pool_address'] = miner_state['result'][7].split(';')

	hash1 = list(map(lambda x: int(x), miner_state['result'][2].split(';')))

	minerlist[hostname]['mining_hash1_sum'] = hash1[0] * 1024
	minerlist[hostname]['mining_hash1'] = list(map(lambda x: int(x) * 1024 if x != 'off' else 0, miner_state['result'][3].split(';')))
	if len(miner_state['result'][6]) > 0:
		minerlist[hostname]['graphics_temperate'] = list(map(lambda x: int(x), miner_state['result'][6].split(';')))

	else:
		minerlist[hostname]['graphics_temperate'] = [0] * len(minerlist[hostname]['mining_hash1']) * 2

	for a in range(len(minerlist[hostname]['graphics_temperate']), len(minerlist[hostname]['mining_hash1']) * 2):
		minerlist[hostname]['graphics_temperate'].append(0)

	minerlist[hostname]['graphics_count'] = len(minerlist[hostname]['mining_hash1'])

	invaild_share = list(map(lambda x: int(x), miner_state['result'][8].split(';')))
	minerlist[hostname]['pool_share'] = [hash1[1], hash1[2], invaild_share[0]]

	if len(minerlist[hostname]['pool_address']) == 2:
		minerlist[hostname]['mining_mode'] = 1
		hash2 = list(map(lambda x: int(x), miner_state['result'][4].split(';')))

		minerlist[hostname]['mining_hash2_sum'] = hash2[0] * 1024
		minerlist[hostname]['mining_hash2'] = list(map(lambda x: int(x) * 1024 if x != 'off' else 0, miner_state['result'][5].split(';')))

		minerlist[hostname]['pool_share'] = [hash1[1], hash1[2], invaild_share[0], hash2[1], hash2[2], invaild_share[2]]

def miner_state_update_ewbf(hostname, miner_state):
	global minerlist

	minerlist[hostname]['miner_state'] = True

	minerlist[hostname]['mining_coin1'] = 102

	minerlist[hostname]['miner_version'] = 'EWBF Zec Miner Version '
	minerlist[hostname]['miner_running_time'] = int(miner_state.get('start_time', 0))
	minerlist[hostname]['pool_address'] = miner_state.get('current_server', '?')

	if minerlist[hostname]['miner_running_time'] == 0:
		minerlist[hostname]['miner_version'] += '>= 0.3.3b'

	else:
		minerlist[hostname]['miner_version'] += '<= 0.3.4b'

        minerlist[hostname]['graphics_count'] = len(miner_state['result'])

	minerlist[hostname]['mining_hash1_sum'] = 0
	minerlist[hostname]['mining_hash1'] = []
	minerlist[hostname]['graphics_temperate'] = []
	minerlist[hostname]['pool_share'] = [0, 0, 0]

	for a in miner_state['result']:
		minerlist[hostname]['mining_hash1_sum'] = minerlist[hostname]['mining_hash1_sum'] + int(a['speed_sps'])
        	minerlist[hostname]['mining_hash1'].append(int(a['speed_sps']))

        	minerlist[hostname]['graphics_temperate'] = minerlist[hostname]['graphics_temperate'] + [int(a['temperature']), '0']

		minerlist[hostname]['pool_share'][0] = minerlist[hostname]['pool_share'][0] + int(a['accepted_shares'])
		minerlist[hostname]['pool_share'][1] = minerlist[hostname]['pool_share'][1] + int(a['rejected_shares'])

def miner_state_update_claymore_monero(hostname, miner_state):
	global minerlist

	miner_state_update_claymore(hostname, miner_state)
	minerlist[hostname]['mining_coin1'] = 103
	minerlist[hostname]['mining_coin2'] = 0

	hash1 = list(map(lambda x: int(x), miner_state['result'][2].split(';')))
	minerlist[hostname]['mining_hash1_sum'] = hash1[0]
	minerlist[hostname]['mining_hash1'] = list(map(lambda x: int(x) if x != 'off' else 0, miner_state['result'][3].split(';')))
