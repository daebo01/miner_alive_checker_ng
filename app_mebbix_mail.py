# -*- coding: utf-8 -*-

import config, util_sendmail

mailqueue = []

mail_struct = {
'recv_user': '',
'subject': '',
'message': '',
'file': None,
}

def event_mail(eventid, miner, starttime = None, endtime = None):
	if starttime == endtime:
		subject = '%s: %s' % (eventid, miner)

	elif endtime is None:
		subject = '-PROBLEM- %s: %s' % (eventid, miner)

	else:
		subject = '-OK- %s: %s' % (eventid, miner)

	message = u'%s<br>%s<br><br>' % (eventid, miner)

	mail_send(config.mail_recv_user, subject, message)

def mail_send(recv_user, subject, message):
	global mailqueue

	newmail = mail_struct.copy()
	newmail['recv_user'] = recv_user
	newmail['subject'] = '[' + config.name + '] ' + subject
	newmail['message'] = message + '<br><br><br><br>' + config.name + ' ' + config.version

	mailqueue.append(newmail)

def mail_queue_send():
	global mailqueue

	while len(mailqueue) != 0:
		mail = mailqueue.pop(0)

		util_sendmail.send_mail(config.smtp_userid, 'gihun@emato.net', mail['recv_user'], mail['subject'], mail['message'], mail['file'])
