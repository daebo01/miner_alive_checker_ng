# -*- coding:utf-8 -*-

import smtplib  
from email.MIMEMultipart import MIMEMultipart  
from email.MIMEBase import MIMEBase  
from email.MIMEText import MIMEText  
from email import Encoders  
from email import Utils  
from email.header import Header  
import os
import config

def send_mail(from_user, to_user, cc_users, subject, text, attach):  
	COMMASPACE = ", "
	msg = MIMEMultipart("alternative")
	msg["From"] = from_user
	msg["To"] = to_user
	msg["Cc"] = COMMASPACE.join(cc_users)
	msg["Subject"] = Header(s=subject, charset="utf-8")
	msg["Date"] = Utils.formatdate(localtime = 1)
	msg.attach(MIMEText(text, "html", _charset="utf-8"))

	if (attach != None):
	        part = MIMEBase("application", "octet-stream")
	        part.set_payload(open(attach, "rb").read())
	        Encoders.encode_base64(part)
	        part.add_header("Content-Disposition", "attachment; filename=\"%s\"" % os.path.basename(attach))
	        msg.attach(part)

	if(config.smtp_port == 465):
		smtp = smtplib.SMTP_SSL(config.smtp_server, config.smtp_port)
	else:
		smtp = smtplib.SMTP(config.smtp_server, config.smtp_port)

	if(config.smtp_port == 587):
		smtp.ehlo()
		smtp.starttls()
	#else:
	#	print("no ssl oonnect")

	smtp.login(config.smtp_userid, config.smtp_passwd)
	smtp.sendmail(from_user, cc_users, msg.as_string())
	smtp.close()
