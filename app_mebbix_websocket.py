# -*- coding: utf-8 -*-

from gevent import sleep

user_remote = {}
user_local = {}

def remote_connection_check(user, hostname):
	global user_remote
	key = '$'.join((user, hostname))

	if user_remote.get(key, None) is None:
		return True

	if user_remote[key].closed is True:
		del(user_remote[key])
		return True

	return False

def remote(ws, user, hostname):
	global user_remote
	key = '$'.join((user, hostname))

	user_remote[key] = ws

	while not ws.closed:
		message = ws.receive()
		if message:
			if user_local.get(key, None):
				if user_local[key].closed is False:
					user_local[key].send(message, binary=True)

	del(user_remote[key])
		
def local(ws, user, hostname):
	global user_local
	key = '$'.join((user, hostname))

	user_local[key] = ws

	if user_remote.get(key, None):
		if user_remote[key].closed is False:
			while not ws.closed:
				message = ws.receive()
				if message:
					user_remote[key].send(message, binary=True)

	ws.close()
	del(user_local[key])

def echo(ws):
	while not ws.closed:
		message = ws.receive()
		if message:
			ws.send(message)
