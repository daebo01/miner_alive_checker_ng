pragma journal_mode = WAL;

create table user
(
	id integer primary key autoincrement,
	user varchar(255) not null,
	pass varchar(255) not null
);

CREATE TABLE miner_event
(
id integer primary key autoincrement,
minerid integer not null,
type integer not null,
starttime integer not null,
endtime integer null,
data1 varchar(255) null,
data2 varchar(255) null
);

CREATE TABLE miner_detail
(
id integer primary key autoincrement,
userid integer not null,
miner varchar(255) not null,
create_time integer not null,
modify_time integer not null,
cpu varchar(255) not null,
board varchar(255) not null,
graphics varchar(255) not null,
ip char(15) not null,
mac char(12) not null,
graphics_max integer not null default 0,
groupid integer not null default 0
);

create table miner_graph
(
id integer primary key autoincrement,
minerid integer not null,
type integer not null,
data varchar(255) not null
);

CREATE TABLE miner_count
(
        id integer primary key autoincrement,
        minerid integer not null,
        type integer not null,
        data integer not null
);

CREATE TABLE user_group
(
	id integer primary key autoincrement,
	userid integer not null,
	name varchar(255) not null
);
