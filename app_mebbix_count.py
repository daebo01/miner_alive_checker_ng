# -*- coding: utf-8 -*-

from database import Database

import app_const

miner_count_struct = {
	'minerid' : 0,
	'typeid' : 0,
	'data' : 0,
}

miner_count = {}

db = None

def init():
	global db

	print('app_mebbix_count init')

	for a in app_const.COUNT_TYPE:
		miner_count[a] = {}

	db = Database()

	for a in db.count_list():
		#id, minerid, type, data
		add(a[1], a[2], a[3])

def get(minerid, typeid):
	global miner_count

	data = miner_count[typeid].get(minerid, None)

	if data is None:
		add(minerid, typeid, 0)
		data = miner_count[typeid][minerid]

	return data

def add(minerid, typeid, data):
	global miner_count

	newcount = miner_count_struct.copy()
	newcount['minerid'] = minerid
	newcount['typeid'] = typeid
	newcount['data'] = data

	miner_count[typeid][minerid] = newcount

def update(minerid, typeid, data):
	global miner_count

	miner_count[typeid][minerid]['data'] = data
	db.count_update(minerid, typeid, data)
