# -*- coding: utf-8 -*- 

def fix_werkzeug_logging():
	from werkzeug.serving import WSGIRequestHandler

	def address_string(self):
		forwarded_for = self.headers.get('X-Forwarded-For', '').split(',')
		if forwarded_for and forwarded_for[0]:
			return forwarded_for[0]
		else:
			return self.client_address[0]

	WSGIRequestHandler.address_string = address_string

def fix_gevent_logging():
	from gevent import pywsgi
	from datetime import datetime

	def format_request(self):
		now = datetime.now().replace(microsecond=0)
		length = self.response_length or '-'
		if self.time_finish:
			delta = '%.6f' % (self.time_finish - self.time_start)
		else:
			delta = '-'
		client_address = self.client_address[0] if isinstance(self.client_address, tuple) else self.client_address
		forwarded_for = self.headers.get('X-Forwarded-For', '').split(',')
		if forwarded_for and forwarded_for[0]:
			client_address = forwarded_for[0]
		return '%s - - [%s] "%s" %s %s %s' % (
			client_address or '-',
			now,
			getattr(self, 'requestline', ''),
			(getattr(self, 'status', None) or '000').split()[0],
			length,
			delta)

	pywsgi.WSGIHandler.format_request = format_request

def fix_gevent_urllib2_error():
	return None

	import inspect
	__ssl__ = __import__('ssl')

	try:
		_ssl = __ssl__._ssl
	except AttributeError:
		_ssl = __ssl__._ssl2


	def new_sslwrap(sock, server_side=False, keyfile=None, certfile=None, cert_reqs=__ssl__.CERT_NONE, ssl_version=__ssl__.PROTOCOL_SSLv23, ca_certs=None, ciphers=None):
		context = __ssl__.SSLContext(ssl_version)
		context.verify_mode = cert_reqs or __ssl__.CERT_NONE
		if ca_certs:
			context.load_verify_locations(ca_certs)
		if certfile:
			context.load_cert_chain(certfile, keyfile)
		if ciphers:
			context.set_ciphers(ciphers)

		caller_self = inspect.currentframe().f_back.f_locals['self']
		return context._wrap_socket(sock, server_side=server_side, ssl_sock=caller_self)

	if not hasattr(_ssl, 'sslwrap'):
		_ssl.sslwrap = new_sslwrap
