# -*- coding: utf-8 -*- 

import os, logging, time, sys, datetime

from gevent import monkey
monkey.patch_all()
from gevent import wsgi
from geventwebsocket.handler import WebSocketHandler
from flask import Flask
from werkzeug.contrib.fixers import ProxyFix
from flask_sockets import Sockets

import config, drop_privileges, util, app_util, app_route, test
import app_login
import app_mebbix_user
import app_mebbix_count
import app_mebbix_agent
import app_mebbix_page
import app_mebbix_event
import app_mebbix_stat
import app_mebbix_websocket

from session import CachingSessionManager, FileBackedSessionManager, ManagedSessionInterface
from miner_check_thread import MinerCheckThread

#reload(sys)
#sys.setdefaultencoding('utf-8')

app = Flask(__name__)
app.wsgi_app = ProxyFix(app.wsgi_app)
sockets = Sockets(app)

def reloadtest():
	reload(app_mebbix_page)
	int('a')
	return 'reloaded'

#route
app.add_url_rule('/service/', '_service', app_route.hello)
app.add_url_rule('/service/reload', '_service_reload', app_route.reloadtest)
app.add_url_rule('/service/reload_', '_service_reload_', reloadtest)

#app_login
app.add_url_rule('/service/login', '_service_login', app_login.login, methods=['POST', 'GET'])
app.add_url_rule('/service/logout', '_service_logout', app_login.logout)
app.add_url_rule('/service/register', '_service_register', app_login.register)

#app_mebbix_page
app.add_url_rule('/service/dashboard', '_service_dashboard', app_mebbix_page.dashboard)

app.add_url_rule('/service/miner', '_service_miner', app_mebbix_page.miner)
app.add_url_rule('/service/miner/<hostname>', '_service_miner_detail', app_mebbix_page.miner_detail)
app.add_url_rule('/service/miner_modify', '_service_miner_modify', app_mebbix_page.miner_modify, methods=['POST'])
app.add_url_rule('/service/miner_delete', '_service_miner_delete', app_mebbix_page.miner_delete, methods=['POST'])
app.add_url_rule('/service/miner_remote/<hostname>', '_service_remote', app_mebbix_page.miner_remote)

app.add_url_rule('/service/event', '_service_event', app_mebbix_page.miner_event)
app.add_url_rule('/service/event/page/<int:page>', '_service_event_page', app_mebbix_page.miner_event)
#app.add_url_rule('/service/event/miner/<hostname>', '_service_event_miner', app_mebbix_page.event_miner)

app.add_url_rule('/service/debugpage', '_service_debugpage', app_mebbix_page.debugpage)

#app_mebbix_agent
app.add_url_rule('/service/agent/alive', '_service_agent_alive', app_mebbix_agent.alive, methods=['POST'])
app.add_url_rule('/service/agent/poweron', '_service_agent_poweron', app_mebbix_agent.poweron, methods=['POST'])
app.add_url_rule('/service/agent/reboot', '_service_agent_reboot', app_mebbix_agent.reboot, methods=['POST'])

#test
app.add_url_rule('/service/test', '_service_test', test.test)

#socket
sockets.add_url_rule('/websocket/echo', '_websocket_echo', app_mebbix_websocket.echo)
sockets.add_url_rule('/websocket/remote/<user>/<hostname>', '_websocket_remote', app_mebbix_websocket.remote)
sockets.add_url_rule('/websocket/local/<user>/<hostname>', '_websocket_local', app_mebbix_websocket.local)

#@app.teardown_appcontext
#def db_close(exception):
#	database.close_connection(exception)


if __name__ == "__main__":
#	print(os.getresuid())
#	drop_privileges.drop_privileges()
#	print(os.getresuid())

	app_util.fix_werkzeug_logging()
	app_util.fix_gevent_logging()
#	app_util.fix_gevent_urllib2_error()

	logger = logging.getLogger('werkzeug')
	handler = logging.FileHandler('access.log')
	logger.addHandler(handler)
	logging.basicConfig(
		filename='access.log',
		format='%(asctime)s %(levelname)s: %(message)s',
		level=logging.INFO)

	log = util.LoggingLogAdapter(logging, logging.INFO)

	app.secret_key = "asdfasdfzxcvzxcvasdfasdfzxcvzxcv1234"
	app.config['SESSION_PATH'] = "./session_dir"
	skip_paths = ['/service/agent']
	app.session_interface = ManagedSessionInterface(CachingSessionManager(FileBackedSessionManager(app.config['SESSION_PATH'], app.config['SECRET_KEY']), 1000), skip_paths, datetime.timedelta(days=1))
	app.app_protocol = lambda x: 'binary' if 'websocket' in x else None

	print('session_dir clear')
	session_file_list = os.listdir(app.config['SESSION_PATH'])
	for a in session_file_list:
		os.unlink(app.config['SESSION_PATH'] + '/' + a)


	#init function
	app_mebbix_user.init()
	app_mebbix_count.init()
	app_mebbix_agent.init()
	app_mebbix_event.init()
	app_mebbix_page.init()
	app_mebbix_stat.init()
	
	th = MinerCheckThread()
	th.daemon = True
	th.start()

	#app.run(config.listen, config.port, debug=True, use_reloader=False, use_debugger=False)
	server = wsgi.WSGIServer((config.listen, config.port), app, handler_class=WebSocketHandler, log=log)
	server.serve_forever()
