# -*- coding: utf-8 -*

from threading import Thread

import time, json

import config
import app_mebbix_mail
import app_mebbix_stat

from database import Database
from util_httpget import url_download

class MinerCheckThread(Thread):
	db = None

	def __init__(self):
		Thread.__init__(self)
		print(u'miner check thread init')
		self.db = Database()

	def run(self):
		while True:
			app_mebbix_stat.update_stat()
			#app_mebbix_mail.miner_alive_mail()
			#app_mebbix_mail.miner_dead_mail()
			#app_mebbix_mail.mail_queue_send()
			time.sleep(60)
