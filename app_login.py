# -*- coding: utf-8 -*-

from flask import session, request, redirect, render_template

import config
def session_check():
	if session.get('login', False) is True:
		return True

	return False

def login():
	if request.method == 'POST':
		password = request.form.get('pass', None)

		if password == config.login_password:
			session['userid'] = 'gihun'
			session['login'] = True
			return redirect('/service/dashboard')

		return redirect('/service/login')

	else:
		dicts = {
			'config' : config,
		}
		return render_template('login.html', dicts = dicts)

def logout():
	session['login'] = False

	return redirect('/')

def register():
	if request.method == 'POST':
		return 'test'

	else:
		dicts = {
			'config' : config,
		}
		return render_template('register.html', dicts = dicts)

