# -*- coding:utf-8 -*-

import urllib2
import socket

def url_download(url):
	headers = { 'User-Agent' : 'Mozilla/5.0 (Windows; U; Windows NT 5.1; it; rv:1.8.1.11) Gecko/20071127 Firefox/2.0.0.11' }
	req = urllib2.Request(url, None, headers)

	try:
		data = urllib2.urlopen(req, timeout=60).read()

	except urllib2.HTTPError as err:
		print("http error : " + str(err.code))
		data = None

	except socket.error, e:
		print('socket error : ' + e)
		data = None

	return data
