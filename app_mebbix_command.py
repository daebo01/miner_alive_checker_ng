# -*- coding: utf-8 -*-

minerlist = {}

def miner_add(id):
	global minerlist

	minerlist[id] = []

def miner_command_add(id, commandid, data):
	global minerlist

	minerlist[id].append({'commandid': commandid, 'data': data})

def miner_command_get(id):
	global minerlist

	ret = minerlist[id]
	minerlist[id] = []
	return ret
