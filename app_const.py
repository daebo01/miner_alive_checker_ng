# -*- coding: utf-8 -*-

"""
100 새로운 마이너
101 전원 켜짐 채굴시작
102 메빅스 꺼짐
103 채굴프로그램 꺼짐
104 PC 정보 변동
105 10분내로 리붓됨

200 그래픽카드 갯수 변동
201 그래픽카드 뜨거움
202 채굴량 너무 적음
"""

EVENT_TYPE = {
100: 'new miner',
101: 'power on',
102: 'timeout',
103: 'miner not working',
104: 'pc info changed',

200: 'graphics count changed',
201: 'graphics hot',
202: 'mining too slow',
}

COIN_TYPE = {
0: 'default',
100: 'Ethereum',
101: 'Pascal',
102: 'ZCash',
103: 'Monero',
}

COUNT_TYPE = {
0: 'default',
100: 'graphics max count',
101: 'memo',
}

COMMAND_TYPE = {
0: 'default',
100: 'remoteview execute',
}
