# -*- coding: utf-8 -*-

import app_const
import app_mebbix_agent

stat = {
'graphics_count': {},
'graphics_max_count': {},
'mining_hash': {},
'online_miner': 0,
'all_miner': 0,
}

def init():
	print('app_mebbix_stat init')

	init_mining_hash()

def update_stat():
	update_graphics_count()
	update_mining_hash()
	update_miner_count()

def update_graphics_count():
	#rx470 series 470d
	#rx470 graphics 470
	#1060 3gb
	#1060 6gb
	#1070

	global stat

	stat['graphics_count'] = {}
	stat['graphics_max_count'] = {}

	for a in app_mebbix_agent.minerlist.keys():
		name = app_mebbix_agent.minerlist[a]['graphics']
		count = app_mebbix_agent.minerlist[a]['graphics_count']
		max_count = app_mebbix_agent.minerlist[a]['graphics_max']['data']
		if stat['graphics_count'].get(name, None) is None:
			stat['graphics_count'][name] = count

		else:
			stat['graphics_count'][name] = stat['graphics_count'][name] + count


		if stat['graphics_max_count'].get(name, None) is None:
			stat['graphics_max_count'][name] = max_count

		else:
			stat['graphics_max_count'][name] = stat['graphics_max_count'][name] + max_count


def init_mining_hash():
	global stat

	del stat['mining_hash']
	stat['mining_hash'] = {}
	for a in app_const.COIN_TYPE.keys():
		#name = app_const.COIN_TYPE[a]
		stat['mining_hash'][a] = 0


def update_mining_hash():
	global stat

	init_mining_hash()

	for a in app_mebbix_agent.minerlist.keys():
		id1 = app_mebbix_agent.minerlist[a]['mining_coin1']
		id2 = app_mebbix_agent.minerlist[a]['mining_coin2']
		stat['mining_hash'][id1] = stat['mining_hash'][id1] + app_mebbix_agent.minerlist[a]['mining_hash1_sum']
		stat['mining_hash'][id2] = stat['mining_hash'][id2] + app_mebbix_agent.minerlist[a]['mining_hash2_sum']

def update_current_exchange():
	1

def update_miner_count():
	global stat

	stat['all_miner'] = len(app_mebbix_agent.minerlist.keys())
	stat['online_miner'] = 0

	for a in app_mebbix_agent.minerlist.keys():
		if app_mebbix_agent.minerlist[a]['miner_state']:
			stat['online_miner'] = stat['online_miner'] + 1

		
