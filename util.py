# -*- coding: utf-8 -*- 

import re, hashlib

def regex_check(regex, data):
	if isinstance(data, unicode):
		patt = re.compile(unicode(regex), re.UNICODE)
	else:
		patt = re.compile(regex)
		
	return bool(patt.match(data))

def regex_match(regex, data):
	patt = re.compile(regex, re.UNICODE)
	return patt.match(data)

class LoggingLogAdapter(object):
	def __init__(self, logger_, level):
		self._logger = logger_
		self._level = level

	def write(self, msg):
		newmsg = msg.rstrip('\r\n')
		self._logger.log(self._level, newmsg)

def sha1_hash(data):
	h = hashlib.sha1()
	if isinstance(data, unicode):
		h.update(data.encode('utf-8'))
	else:
		h.update(data)

	return h.hexdigest()

def pretty_date(time=False, time2=False):
    """
    Get a datetime object or a int() Epoch timestamp and return a
    pretty string like 'an hour ago', 'Yesterday', '3 months ago',
    'just now', etc
    """
    from datetime import datetime
    now = datetime.now()
    if type(time) is int and type(time2) is int:
        diff = datetime.fromtimestamp(time2 - time)
    elif type(time) is int:
        diff = now - datetime.fromtimestamp(time)
    elif isinstance(time,datetime):
        diff = now - time
    elif not time:
        diff = now - now
    second_diff = diff.seconds
    day_diff = diff.days

    if day_diff < 0:
        return ''

    if day_diff == 0:
        if second_diff < 10:
            return "just now"
        if second_diff < 60:
            return str(second_diff) + " seconds ago"
        if second_diff < 120:
            return "a minute ago"
        if second_diff < 3600:
            return str(second_diff / 60) + " minutes ago"
        if second_diff < 7200:
            return "an hour ago"
        if second_diff < 86400:
            return str(second_diff / 3600) + " hours ago"
    if day_diff == 1:
        return "Yesterday"
    if day_diff < 7:
        return str(day_diff) + " days ago"
    if day_diff < 31:
        return str(day_diff / 7) + " weeks ago"
    if day_diff < 365:
        return str(day_diff / 30) + " months ago"
    return str(day_diff / 365) + " years ago"

def human_read_number(num):
	"""Human friendly file size"""
	from math import log
	unit_list = zip(['h/s', 'Kh/s', 'Mh/s', 'Gh/s', 'Th/s', 'Ph/s'], [0, 2, 2, 2, 2, 2])

	if num > 1:
		exponent = min(int(log(num, 1024)), len(unit_list) - 1)
		quotient = float(num) / 1024**exponent
		unit, num_decimals = unit_list[exponent]
		format_string = '{:.%sf} {}' % (num_decimals)
		return format_string.format(quotient, unit)

	else:
		return str(num) + ' h/s'

