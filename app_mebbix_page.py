# -*- coding: utf-8 -*-

import time, datetime

from flask import render_template, redirect, request, session

import config
import util
import app_login
import app_const
import app_mebbix_agent
import app_mebbix_event
import app_mebbix_stat
import app_mebbix_count
import app_mebbix_websocket
import app_mebbix_command

from database import Database

db = None

def init():
	global db

	print('app_mebbix_page init')
	db = Database()

def def_dicts():
	dicts = {
		'config' : config,
		'util' : {
			'pretty_date' : util.pretty_date,
			'fromtimestamp' : datetime.datetime.fromtimestamp,
			'human_read_number' : util.human_read_number, 
		},
		'time' : int(time.time()),

		'const': app_const,

		'miner_stat' : app_mebbix_stat.stat,
	
		'miner' : app_mebbix_agent.minerlist,
		'minerid' : app_mebbix_agent.mineridlist,
	}

	return dicts

def dashboard():
	if app_login.session_check() is False:
		return redirect('/')

	dicts = def_dicts()

	return render_template('dashboard.html', dicts = dicts)

def miner():
	if app_login.session_check() is False:
		return redirect('/')

	dicts = def_dicts()
	dicts['miner_key'] = sorted(app_mebbix_agent.minerlist.keys())

	return render_template('miner.html', dicts = dicts)

def miner_detail(hostname):
	if app_login.session_check() is False:
		return redirect('/')

	dicts = def_dicts()
	dicts['hostname'] = hostname

	if hostname in app_mebbix_agent.minerlist.keys():
		return render_template('miner_detail.html', dicts = dicts)

	else:
		dicts['error'] = u'없는 마이너입니다!!!'
		return render_template('error.html', dicts = dicts)


def miner_event(page = 0):
	if app_login.session_check() is False:
		return redirect('/')

	dicts = def_dicts()
	dicts['event'] = db.event_get(page * 100)

	return render_template('event.html', dicts=dicts)

def miner_modify():
	if app_login.session_check() is False:
		return redirect('/')

	dicts = def_dicts()

	try:
		minerid = int(request.form.get('id', None))

	except (TypeError, ValueError):
		minerid = None

	newname = request.form.get('name', None)

	try:
		graphics_max = int(request.form.get('graphics_max', None))
		memo = request.form.get('memo', '')

	except (TypeError, ValueError):
		graphics_max = None

	if minerid is None or not newname or graphics_max is None:
		dicts['error'] = u'파라메터에 문제가 있슴미다\n나쁜 짓은 나빠요'
		return render_template('error.html', dicts = dicts)

	else:
		minername = app_mebbix_agent.mineridlist.get(minerid, None)
		if minername:
			#app_mebbix_agent.minerlist[newname] = app_mebbix_agent.minerlist[minername]
			#app_mebbix_agent.mineridlist[minerid] = newname
			#del app_mebbix_agent.minerlist[minername]
			app_mebbix_count.update(minerid, 100, graphics_max)
			app_mebbix_count.update(minerid, 101, memo)

			return redirect('/service/miner/' + minername)

		else:
			return render_template('error.html', dicts = {'error': u'없는 마이너입니다'})

def miner_delete():
	if app_login.session_check() is False:
		return redirect('/')

	try:
		minerid = int(request.form.get('id', None))

	except (TypeError, ValueError):
		minerid = None

	minername = app_mebbix_agent.mineridlist.get(minerid, None)

	if minerid is None or minerid == 0 or minername is None:
		return render_template('error.html', dicts = {'error': u'없는 마이너입니다'})

	else:
		#마이너 삭제 이벤트
		app_mebbix_agent.miner_delete(minername)
		return redirect('/service/miner')

def miner_remote(hostname):
	if app_login.session_check() is False:
		return redirect('/')

	dicts = def_dicts()
	dicts['hostname'] = hostname

	if hostname in app_mebbix_agent.minerlist.keys():
		if app_mebbix_websocket.remote_connection_check(session['userid'], hostname):
			app_mebbix_command.miner_command_add(app_mebbix_agent.minerlist[hostname]['id'], 100, True)
			return render_template('remote.html', dicts = dicts)

		else:
			dicts['error'] = u'이미 접속중입니다'
			return render_template('error.html', dicts = dicts)

	else:
		dicts['error'] = u'없는 마이너입니다!!!'
		return render_template('error.html', dicts = dicts)


def debugpage():
	if app_login.session_check() is False:
		return redirect('/')

	dicts = def_dicts()

	return render_template('debugpage.html', dicts = dicts)
