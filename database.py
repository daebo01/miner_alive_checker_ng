# -*- coding: utf-8 -*-
#http://flask.pocoo.org/docs/0.11/patterns/sqlite3/

import sqlite3
import time

import config, util

class Database:
	DATABASE = config.dbfilename

	database_session = None

	def __init__(self):
		print('database init')

		self.database_session = sqlite3.connect(self.DATABASE, check_same_thread=False)
#		self.database_session.execute("PRAGMA journal_mode=WAL")

	def get_db(self):
		#db = getattr(g, '_database', None)
		if self.database_session is None:
		#	db = g._database = _database
			self.init()
		return self.database_session

	def miner_detail_get(self):
		db = self.get_db()
		cur = db.execute('select * from miner_detail')
		return cur.fetchall()

	def miner_detail_add(self, hostname, minerinfo):
		db = self.get_db()
		db.execute('insert into miner_detail (miner, create_time, modify_time, cpu, board, graphics, ip, mac) values(?, ?, ?, ?, ?, ?, ?, ?)', [hostname, int(time.time()), int(time.time()), minerinfo['cpu'], minerinfo['board'], minerinfo['graphics'], minerinfo['ip'], minerinfo['mac']])
		db.commit()

	def miner_detail_update(self, hostname, minerinfo):
		db = self.get_db()
		db.execute('update miner_detail set modify_time = ?, cpu = ?, board = ?, graphics = ?, ip = ?, mac = ? where miner = ?', [int(time.time()), minerinfo['cpu'], minerinfo['board'], minerinfo['graphics'], minerinfo['ip'], minerinfo['mac'], hostname])
		db.commit()

	def miner_detail_get_id_by_name(self, hostname):
		db = self.get_db()
		cur = db.execute('select id from miner_detail where miner = ?', [hostname])
		return cur.fetchone()

	def event_add(self, minerid, type, starttime, endtime = None, data1 = None, data2 = None):
		db = self.get_db()
		db.execute('insert into miner_event (minerid, type, starttime, endtime, data1, data2) values (?, ?, ?, ? ,?, ?)', [minerid, type, starttime, endtime, data1, data2])
		db.commit()

	def event_get(self, start=0, limit=100):
		db = self.get_db()
		cur = db.execute('select * from miner_event limit ?, ?',  [start, limit])
		return cur.fetchall()

	def count_list(self):
		db = self.get_db()
		cur = db.execute('select * from miner_count')
		return cur.fetchall()

#	def count_get(self, minerid, type):
#		db = self.get_db()
#		cur = db.execute('select data from miner_count where minerid = ? and type = ?', (minerid, type))
#		return cur.fetchone()

	def count_update(self, minerid, type, data):
		db = self.get_db()
		db.execute('insert or replace into miner_count(minerid, type, data) values (?, ?, ?)', (minerid, type, data))
		db.commit()

	def miner_delete(self, minerid):
		db = self.get_db()
		db.execute('delete from miner_event where minerid = ?', (minerid, ))
		db.commit()
		db.execute('delete from miner_graph where minerid = ?', (minerid, ))
		db.commit()
		db.execute('delete from miner_count where minerid = ?', (minerid, ))
		db.commit()
		db.execute('delete from miner_detail where id = ?', (minerid, ))
		db.commit()

	def user_get(self):
		db = self.get_db()
		cur = db.execute('select * from user')
		return cur.fetchall()

	def user_group_get(self):
		db = self.get_db()
		cur = db.execute('select * from user_group')
		return cur.fetchall()
